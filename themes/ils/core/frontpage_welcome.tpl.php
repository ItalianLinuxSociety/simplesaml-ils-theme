<?php
/**
 * Frontpage Welcome Template.
 *
 * The main entry page.
 *
 * @author     Cory Collier <corycollier@corycollier.com>
 * @license    http://opensource.org/licenses/MIT  MIT License
 * @version    git: $Id$
 * @link       https://github.com/corycollier/simplesamlphp-module-themes
 * @see        https://github.com/simplesamlphp/simplesamlphp/
 * @since      File available since Release 1.3.0
 */
?>
<?php
$this->data['header'] = $this->t('{core:frontpage:page_title}');
$this->includeAtTemplateBase('includes/header.php');
?>

<div class="row">
        <div class="col-md-12">
            <div>
                <p>
                    Questa è una pagina di servizio, utilizzata per accedere ad alcuni dei siti di Italian Linux Society.
                </p>
                <ul>
                    <li>
                        <a href="https://forum.linux.it/">Forum</a>: il forum di Italian Linux Society.
                    </li>
                    <li>
                        <a href="https://servizi.linux.it/">Servizi</a>: applicazioni web, libere e gratuite di utilità generale.
                    </li>
                    <li>
                        <a href="https://fosdem.linux.it/">FOSDEM Extended</a>: la più grande conferenza europea dedicata all'opensource, a casa tua.
                    </li>
                    <li>
                        <a href="https://eventi.merge-it.net/">Eventi</a>: il portale per creare eventi ed utilizzare BigBlueButton e registrare gli incontri.
                    </li>
                </ul>
                
                Non dimentichiamoci del <a href="https://planet.linux.it/" target="_blank">Planet</a>, l'unico sito dove puoi trovare aggregate tutte le notizie dai LUG oltre a tutti gli eventi (con calendario) in Italia del mondo FOSS divisi per regione senza nessun accesso!
                
                <hr>
                <?php
                // fillme è un honeypot per lo spam
                use PHPMailer\PHPMailer\PHPMailer;
                use PHPMailer\PHPMailer\Exception;
                include \SimpleSAML\Utils\Config::getConfigDir() . DIRECTORY_SEPARATOR . 'config.php';

                if (empty($_POST['fillme']) && isset($_POST['email'])) {
                    $db = new mysqli('localhost', $config['database.username'], $config['database.password'], str_replace( 'mysql:host=localhost;dbname=', '', $config['database.dsn'] ));
                    $mail_address = $db->real_escape_string($_POST['email']);
                    $mail_address = filter_var($mail_address, FILTER_VALIDATE_EMAIL);
                    if ($mail_address === false) {
                        ?>

                        <div class="alert alert-danger">
                            Indirizzo mail non valido!
                        </div>

                        <?php
                    }
                    else {
                        $mail_body = null;
                        $feedback = null;

                        $query = sprintf("SELECT * FROM pending WHERE token = '%s'", 'questo token è volutamente rotto');
                        $rs = $db->query($query);

                        if($rs->num_rows == 0) {
                            $token = '';
                            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                            for($i = 0; $i < 20; $i++)
                                $token .= $characters[rand(0, strlen($characters) - 1)];
                            $query = sprintf("INSERT INTO pending (email, date, token) VALUES ('%s', NOW(), '%s')", $mail_address, $token);
                            $db->query($query);

                            $query = sprintf("SELECT * FROM AttributeFromSQL WHERE attribute = 'email' AND value = '%s'", $mail_address);
                            $rs = $db->query($query);
                            if ($rs->num_rows != 0) {
                                $mail_body = sprintf("Per accedere devi usare il tuo nickname generato dall'email\nPer modificare la tua password (non supporta i simboli e deve essere meno di 16 caratteri) su login.servizi.linux.it visita l'indirizzo\nhttps://login.servizi.linux.it/module.php/core/frontpage_welcome.php?token=%s&action=change\n\nPer comunicazioni o segnalazioni, scrivi a webmaster@linux.it\n", $token);
                                $feedback = "Ti è stata inviata una mail per aggiornare la tua password.";
                            }
                        }
                        else {
                            $token = $rs->fetch_assoc()['token'];
                        }

                        if ($mail_body == null)
                            $mail_body = sprintf("Conferma la tua registrazione su login.servizi.linux.it visitando l'indirizzo\nhttps://login.servizi.linux.it/module.php/core/frontpage_welcome.php?token=%s&action=change\n\nPer comunicazioni o segnalazioni, scrivi a webmaster@linux.it\n", $token);
                        if ($feedback == null)
                            $feedback = "Ti è stata inviata una mail di conferma. Grazie per la registrazione.";

                        $mail = new PHPMailer(true);
                        $mail->isSMTP();
                        $mail->Host = $config['mail.transport.options']['host'];
                        $mail->SMTPAuth = true;
                        $mail->Username = $config['mail.transport.options']['username'];
                        $mail->Password = $config['mail.transport.options']['password'];
                        $mail->SMTPSecure = 'tls';
                        $mail->Port = 587;

                        $mail->setFrom('nonrispondere@linux.it', 'ILS');
                        $mail->addAddress($mail_address);

                        $mail->isHTML(false);
                        $mail->Subject = 'Conferma il tuo account ILS';
                        $mail->Body = $mail_body;

                        try {
                            $mail->send();
                        }
                        catch(Exception $e) {
                            $feedback = sprintf("Si è verificato un errore nell'invio della mail.<br><br>%s", nl2br($mail_body));
                        }

                        ?>

                        <div class="alert alert-success">
                            <?php echo $feedback ?>
                        </div>

                        <?php
                    }
                } elseif(empty($_POST['fillme']) && isset($_REQUEST['token'])) {
                    $db = new mysqli('localhost', $config['database.username'], $config['database.password'], str_replace( 'mysql:host=localhost;dbname=', '', $config['database.dsn'] ));
                    $token = $db->real_escape_string($_REQUEST['token']);

                    $query = sprintf("SELECT * FROM pending WHERE token = '%s'", $token);
                    $rs = $db->query($query);
                    if ($rs->num_rows == 0) {
                        ?>

                        <div class="alert alert-danger">
                            Token non valido!
                        </div>

                        <?php
                    } else {
                        $pending = $rs->fetch_assoc();
                        $email = $pending['email'];

                        if (isset($_POST['action'])) {
                            if ($_POST['action'] == 'save') {
                                $username = $db->real_escape_string($_REQUEST['username']);
                                $name = $db->real_escape_string($_REQUEST['displayName']);
                                $password = $db->real_escape_string($_REQUEST['password']);

                                $query = sprintf("INSERT INTO users (username, password) VALUES ('%s', AES_ENCRYPT('%s', '%s'))", $username, $password, $config['database.password']);
                                $db->query($query);

                                $query = sprintf("INSERT INTO AttributeFromSQL (uid, sp, attribute, value) VALUES ('%s', '%%', 'displayName', '%s')", $username, $name);
                                $db->query($query);
                                $query = sprintf("INSERT INTO AttributeFromSQL (uid, sp, attribute, value) VALUES ('%s', '%%', 'email', '%s')", $username, $email);
                                $db->query($query);
                                $query = sprintf("INSERT INTO AttributeFromSQL (uid, sp, attribute, value) VALUES ('%s', '%%', 'password', '')", $username);
                                $db->query($query);

                                $query = sprintf("DELETE FROM pending WHERE token = '%s'", $token);
                                $db->query($query);

                                ?>

                                <div class="alert alert-success">
                                    Registrazione completata <?php echo $username ?>! Accedi adesso ai <a href="//servizi.linux.it/">Servizi ILS</a>.
                                </div>

                                <?php
                            }
                            else if ($_POST['action'] == 'update') {
                                $query = sprintf("SELECT * FROM pending WHERE token = '%s'", $_REQUEST['token']);
                                $rs = $db->query($query);
                                $pending = $rs->fetch_assoc();
                                $email = $pending['email'];
                                $query = sprintf("SELECT * FROM AttributeFromSQL WHERE attribute = 'email' AND value = '%s'", $email);
                                $check = $db->query($query);
                                $username = $check->fetch_assoc()['uid'];

                                $password = $db->real_escape_string($_REQUEST['password']);

                                $query = sprintf("UPDATE users SET password = AES_ENCRYPT('%s', '%s') WHERE username = '%s'", $password, $config['database.password'], $username);
                                $db->query($query);

                                ?>

                                <div class="alert alert-success">
                                    Password aggiornata <?php echo $username ?>! Accedi adesso ai <a href="//servizi.linux.it/">Servizi ILS</a>.
                                </div>

                                <?php

                            }
                        }

                        if (isset($_GET['action']) && $_GET['action'] == 'change') {
                                $query = sprintf("SELECT * FROM pending WHERE token = '%s'", $_REQUEST['token']);
                                $rs = $db->query($query);
                                $pending = $rs->fetch_assoc();
                                $email = $pending['email'];
                                $query = sprintf("SELECT * FROM AttributeFromSQL WHERE attribute = 'email' AND value = '%s'", $email);
                                $check = $db->query($query);

                                if ($check->num_rows == 0) {
                                    ?>

                                    <form class="form-horizontal" method="POST" action="/module.php/core/frontpage_welcome.php">
                                        <input type="hidden" name="token" value="<?php echo $token ?>">
                                        <input type="hidden" name="action" value="save">
                                        <b>La password non supporta simboli e deve essere più corta di 16 caratteri!</b>
                                        <div class="form-group">
                                            <label for="username" class="col-sm-2 control-label">Username</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="username" name="username" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="displayName" class="col-sm-2 control-label">Nome e Cognome</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="displayName" name="displayName" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="col-sm-2 control-label">Password</label>
                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="password" name="password" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-default">Salva</button>
                                            </div>
                                        </div>
                                    </form>

                                    <?php
                                }
                                else {
                                    ?>

                                    <form class="form-horizontal" method="POST" action="/module.php/core/frontpage_welcome.php">
                                        <input type="hidden" name="token" value="<?php echo $token ?>">
                                        <input type="hidden" name="action" value="update">

                                        <b>La password non supporta simboli e deve essere più corta di 16 caratteri!</b>
                                        <div class="form-group">
                                            <label for="password" class="col-sm-2 control-label">Nuova Password</label>
                                            <div class="col-sm-10">
                                                <input type="password" class="form-control" id="password" name="password" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" class="btn btn-default">Salva</button>
                                            </div>
                                        </div>
                                    </form>

                                    <?php
                                }
                            }
                    }
                } else {
                    // In caso di home page così non ci si confonde
                ?>
                <p>
                    <b>Registrarsi a questo portale non significa associarsi a <a href="https://www.ils.org/">ILS</a>!</b>
                    Per <b>creare un account</b>, o <b>recuperare la tua password</b>, inserisci il tuo indirizzo mail (riceverai un email):
                </p>

                <p>
                    <form method="POST" action="frontpage_welcome.php">
                        <div class="form-group">
                            <input name="email" type="email" class="form-control">
                            <input name="fillme" type="hidden" value="" class="form-control">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default">Invia</button>
                        </div>
                    </form>
                </p>

                <?php } ?>
            </div>
</div>
</div>

<?php $this->includeAtTemplateBase('includes/footer.php');
