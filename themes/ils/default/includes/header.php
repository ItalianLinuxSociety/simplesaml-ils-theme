<?php

/**
 * Support the htmlinject hook, which allows modules to change header, pre and post body on all pages.
 */
$this->data['htmlinject'] = [
    'htmlContentPre' => [],
    'htmlContentPost' => [],
    'htmlContentHead' => [],
];

$jquery = [];
if (array_key_exists('jquery', $this->data)) {
    $jquery = $this->data['jquery'];
}

if (array_key_exists('pageid', $this->data)) {
    $hookinfo = [
        'pre' => &$this->data['htmlinject']['htmlContentPre'],
        'post' => &$this->data['htmlinject']['htmlContentPost'],
        'head' => &$this->data['htmlinject']['htmlContentHead'],
        'jquery' => &$jquery,
        'page' => $this->data['pageid']
    ];

    SimpleSAML\Module::callHooks('htmlinject', $hookinfo);
}
// - o - o - o - o - o - o - o - o - o - o - o - o -

/**
 * Do not allow to frame SimpleSAMLphp pages from another location.
 * This prevents clickjacking attacks in modern browsers.
 *
 * If you don't want any framing at all you can even change this to
 * 'DENY', or comment it out if you actually want to allow foreign
 * sites to put SimpleSAMLphp in a frame. The latter is however
 * probably not a good security practice.
 */

header('X-Frame-Options: SAMEORIGIN');

?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script type="text/javascript" src="/<?php echo $this->data['baseurlpath']; ?>resources/script.js"></script>
<title><?php

if (strpos($_SERVER['REQUEST_URI'],'frontpage_welcome.php') !== false) {
    echo 'Accesso SAML di Linux.it';
} else if (array_key_exists('header', $this->data)) {
    echo $this->data['header'];
} else {
    echo 'SimpleSAMLphp';
}
?></title>

    <!-- include bootstrap -->
    <link rel='stylesheet' href='https://www.linux.it/shared/?f=bootstrap.css' type='text/css' media='all' />
    <link rel='stylesheet' href='https://www.linux.it/shared/?f=main.css' type='text/css' media='all' />
    <link rel='stylesheet' href='https://www.linux.it/assets/style.css' type='text/css' media='all' />

    <script type="text/javascript" src="https://www.linux.it/shared/index.php?f=jquery.js" defer></script>
    <script type="text/javascript" src="https://www.linux.it/shared/index.php?f=bootstrap.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

    <link rel="icon" type="image/icon" href="/<?php echo $this->data['baseurlpath']; ?>resources/logo.png" />
    <style type="text/css">
        .btn {
            margin: 20px auto;
            border-radius: 50px;
            background-color: #ffc928;
            color: #333 !important;
            text-transform: uppercase;
            width: 100%;
            text-align: center;
        }
        .container table td {
            width: 150px;
        }
        .loginicon {
            width: 0 !important;
        }
    </style>
<?php

if (!empty($jquery)) {
    $version = '1.8';
    if (array_key_exists('version', $jquery)) {
        $version = $jquery['version'];
    }

    if ($version == '1.8') {
        if (isset($jquery['core']) && $jquery['core']) {
            echo '<script type="text/javascript" src="/'.$this->data['baseurlpath'].'resources/jquery-1.8.js"></script>'."\n";
        }

        if (isset($jquery['ui']) && $jquery['ui']) {
            echo '<script type="text/javascript" src="/'.$this->data['baseurlpath'].'resources/jquery-ui-1.8.js"></script>'."\n";
        }
    }
}

if (isset($this->data['clipboard.js'])) {
    echo '<script type="text/javascript" src="/'.$this->data['baseurlpath'].'resources/clipboard.min.js"></script>'."\n";
}


if (!empty($this->data['htmlinject']['htmlContentHead'])) {
    foreach ($this->data['htmlinject']['htmlContentHead'] as $c) {
        echo $c;
    }
}

if ( !empty($this->data['extraHeadContent']) ) {
    foreach ($this->data['extraHeadContent'] as $c) {
	echo $c;
    }
}

if ($this->isLanguageRTL()) {
    ?>
    <link rel="stylesheet" type="text/css" href="/<?php echo $this->data['baseurlpath']; ?>resources/default-rtl.css" />
<?php
}
?>
    <meta name="robots" content="noindex, nofollow" />
<?php
if (array_key_exists('head', $this->data)) {
    echo '<!-- head -->'.$this->data['head'].'<!-- /head -->';
}
?>
</head>
<?php
$onLoad = '';
if (array_key_exists('autofocus', $this->data)) {
    $onLoad .= ' onload="SimpleSAML_focus(\''.$this->data['autofocus'].'\');"';
}
?>
<body<?php echo $onLoad; ?>>
    <?php

    $includeLanguageBar = false;
    if (!empty($_POST)) {
        $includeLanguageBar = false;
    }
    if (isset($this->data['hideLanguageBar']) && $this->data['hideLanguageBar'] === true) {
        $includeLanguageBar = false;
    }

    if ($includeLanguageBar) {
        $languages = $this->getLanguageList();
        ksort($languages);
        if (count($languages) > 1) {
            echo '<div id="languagebar">';
            $langnames = [
                'it' => 'Italiano', // Italian
            ];

            $textarray = [];
            foreach ($languages as $lang => $current) {
                $lang = strtolower($lang);
                if ($current) {
                    $textarray[] = $langnames[$lang];
                } else {
                    $textarray[] = '<a href="'.htmlspecialchars(
                        \SimpleSAML\Utils\HTTP::addURLParameters(
                            \SimpleSAML\Utils\HTTP::getSelfURL(),
                            [$this->getTranslator()->getLanguage()->getLanguageParameterName() => $lang]
                        )
                    ).'">'.$langnames[$lang].'</a>';
                }
            }
            echo join(' | ', $textarray);
            echo '</div>';
        }
    }
    ?>

<?php

$config = \SimpleSAML\Configuration::getInstance();
if(! $config->getBoolean('production', true)) {
    echo '<div class="caution">' . $this->t('{preprodwarning:warning:warning}'). '</div>';
}
?>
<div id="header">
	<img src="https://www.linux.it/assets/images/logo.png" alt="Logo Linux">
	<div id="maintitle">Login</div>
	<div id="payoff">Accesso unico ai servizi ILS</div>
</div>
<div class="container mt-5 page-contents">
